<?php

namespace Valhalla\CoreUtilsBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DependencyInjection\SimpleXMLElement;
use Valhalla\CoreUtilsBundle\Classes\Navigation;

class SitemapCommand extends ContainerAwareCommand {

    const BASE_URL = 'http://core-utils.halls-of-valhalla.org';
    const SITEMAP_NAME = 'sitemap.xml';

    private $router;
    private $links;
    private $rootDirectory;
    private $sitemapXml;
    private $saveLocation;

    protected function configure() {
        $this
            ->setName('sitemap:generate')
            ->setDescription('Generate a sitemap');
    }

    protected function init() {
        $this->rootDirectory = $this->getContainer()->get('kernel')->getRootDir() . '/..';
        $this->saveLocation = $this->rootDirectory . '/web/' . self::SITEMAP_NAME;
        $this->router = $this->getContainer()->get('router');
        //You should also set your database connection in here if necessary.
        $this->links = array();
    }

    protected function execute(InputInterface $input, OutputInterface $output) {
        $this->init();
        $output->writeln('Beginning sitemap generation');
        $output->writeln('Generating list of URLs...');
        $this->generateLinks();
        $output->writeln('Generating XML file...');
        $this->writeXML($this->links);
        $output->writeln('Complete!');
    }

    /****************************************************** */

    protected function generateLinks() {
        $this->links[] = array('loc' => self::BASE_URL);
        
        $this->getDocumentationAndPagesLinks();
    }

    protected function getDocumentationAndPagesLinks() {
        $navigation = new Navigation($this->rootDirectory);
        $navigationData = $navigation->getNavigation($this->router);
        
        foreach($navigationData['pages'] as $value){
            $this->links[] = array('loc' => self::BASE_URL . $value['url']);
        }
        
        foreach($navigationData['documentation'] as $sectionName => $section){
            $this->links[] = array('loc' => $this->generateURL('valhalla_core_utils_documentation', array('section' => $sectionName, 'content' => 'overview')));
            foreach($section as $content){
                $this->links[] = array('loc' => self::BASE_URL . $content['url']);
            }
        }
    }

    /****************************************************** */

    protected function writeXML($list) {
        $this->initAppXmlObj();
        $this->updateXmlFile($list);
    }

    /**
     * Appends a row to a specific xml object
     *
     * @param SimpleXMLElement $xmlElement xml object for appending data
     * @param array $array row to add to the xml element
     * @return SimpleXMLElement
     */
    protected function addRow(SimpleXMLElement $xmlElement, array $array) {
        $record = $xmlElement->addChild('url');
        //currently only sets 'loc', but the script could be easily adapted to add additional parameters
        $record->addChild('loc', htmlspecialchars($array['loc']));
        return $xmlElement;
    }

    /**
     * Updates the Xml object laying in $this->saveLocation
     *
     * @param array $rows an array of sitemap links
     */
    protected function updateXmlFile(array $rows) {
        foreach ($rows as $rowArray) {
            $this->addRow($this->sitemapXml, $rowArray);
        }
        $dom = dom_import_simplexml($this->sitemapXml)->ownerDocument;
        $dom->formatOutput = true;
        file_put_contents($this->saveLocation, $dom->saveXML());
    }

    /**
     * Initiates the appXml object
     * @param InputInterface $input
     */
    private function initAppXmlObj() {
        $this->sitemapXml = new SimpleXMLElement('<urlset/>');
        $this->sitemapXml->addAttribute('xmlns', 'http://www.sitemaps.org/schemas/sitemap/0.9');
    }

    private function generateURL($route, $params = array()) {
        return self::BASE_URL . $this->router->generate($route, $params);
    }

}
