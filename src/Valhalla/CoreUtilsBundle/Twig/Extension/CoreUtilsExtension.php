<?php
namespace Valhalla\CoreUtilsBundle\Twig\Extension;

use Valhalla\CoreUtilities\Data\Url;

class CoreUtilsExtension extends \Twig_Extension {
    
    /**
     * {@inheritdoc}
     */
    public function getFilters()
    {
        return array();
    }

    /**
     * {@inheritdoc}
     */
    public function getFunctions() {
        return array(
            'unslugify' => new \Twig_Function_Method($this, 'unslugify'),
        );
    }
    
    public function unslugify($slug){
        return Url::unslugify($slug);
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'coreUtils_extension';
    }
}
