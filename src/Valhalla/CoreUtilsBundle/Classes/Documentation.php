<?php

namespace Valhalla\CoreUtilsBundle\Classes;

use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class Documentation {

    const DOCUMENTATION_ROOT = 'vendor/valhalla/valhalla-core-utils-lib/documentation';

    private $_documentationPath;

    /**
     * @param string $rootDir
     */
    public function __construct($rootDir) {
        $this->_documentationPath = $rootDir . DIRECTORY_SEPARATOR . self::DOCUMENTATION_ROOT;
    }
    
    /**
     * Look up a page
     * 
     * @param string $page
     * @return string
     * @throws NotFoundHttpException
     */
    public function getPage($page){
        $path = $this->_documentationPath . DIRECTORY_SEPARATOR . $page . '.md';
        if(file_exists($path) and is_file($path)){
            return $this->getPrettyMarkup($path);
        } else {
            //try to see if maybe it was a directory instead of a file
            $path = $path = $this->_documentationPath . DIRECTORY_SEPARATOR . $page . DIRECTORY_SEPARATOR . 'overview.md';
            if(file_exists($path) and is_file($path)){
                return $this->getPrettyMarkup($path);
            } else {
                throw new NotFoundHttpException('Invalid page path.');
            }
        }
    }
    
    /**
     * Look up the documentation for a route with the format {section}/{content}
     * 
     * @param string $section
     * @param string $content
     * @return string
     * @throws NotFoundHttpException
     */
    public function getSectionDocumentation($section, $content){
        $path = $this->_documentationPath . DIRECTORY_SEPARATOR . 'docs' . DIRECTORY_SEPARATOR . $section . DIRECTORY_SEPARATOR . $content . '.md';
        if(file_exists($path) and is_file($path)){
            return $this->getPrettyMarkup($path);
        } else {
            throw new NotFoundHttpException('Invalid documentation path.');
        }
    }
    
    /**
     * Open the file at the specified path and parse the markdown
     * 
     * @param string $path
     * @return string
     */
    protected function readFile($path) {
        $parsedown = new \Parsedown();
        $data = file_get_contents($path);

        return $parsedown->text($data);
    }

    /**
     * Get the parsed markdown and reformat any code blocks to have syntax highlighting.
     * 
     * @param string $path
     * @return string
     */
    protected function getPrettyMarkup($path) {
        $markup = $this->readFile($path);
        
        $markup = preg_replace_callback('/<code>(.*?)<\/code>/s', function($matches) {
            $match = html_entity_decode($matches[1]);
            $highlightedText = highlight_string('<?php' . $match . '?>', true);
            $highlightedText = str_replace('&lt;?php', '', $highlightedText);
            $highlightedText = str_replace('?&gt;', '', $highlightedText);
            return $highlightedText;
        }, $markup);
        
        return $markup;
    }

}
