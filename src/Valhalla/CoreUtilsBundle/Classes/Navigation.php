<?php
namespace Valhalla\CoreUtilsBundle\Classes;

use Valhalla\CoreUtilsBundle\Classes\Documentation;
use Valhalla\CoreUtilities\Data\FileSystem;

class Navigation {
    private $_documentationPath;
    private $_fileList;

    /**
     * @param string $rootDir
     */
    public function __construct($rootDir) {
        $this->_documentationPath = $rootDir . DIRECTORY_SEPARATOR . Documentation::DOCUMENTATION_ROOT;
    }
       
    /**
     * @param \Symfony\Component\Routing\Router $router
     * @return array
     */
    public function getNavigation($router){
        $this->_fileList = FileSystem::listFiles($this->_documentationPath);
        
        $docsList = array();
        $docsList['pages'] = $this->getTopLevelDocs($router);
        $docsList['documentation'] = $this->getDocumentationList($router);
        
        return $docsList;
    }
    
    /**
     * 
     * @param \Symfony\Component\Routing\Router $router
     * @return array
     */
    protected function getTopLevelDocs($router){
        $topLevelDocs = array();
        
        foreach($this->_fileList as $key => $file){
            if($file != 'overview.md'){ //overview is automatically included
                $filename = is_array($file) ? $key : basename($file, '.md');
                $topLevelDocs[$filename] = array(
                    'name' => $filename,
                    'url' => $router->generate('valhalla_core_utils_pages',
                            array(
                                'page' => $filename,
                            )
                    ),
                );
            }
        }
        
        return $topLevelDocs;
    }
    
    /**
     * @param \Symfony\Component\Routing\Router $router
     * @return array
     */
    protected function getDocumentationList($router){
        $sectionDocs = array();
        
        //Only allowing depth of 2 for now due to routing
        foreach($this->_fileList['docs'] as $key => $file){
            if(is_array($file)){ //only accept directories; skip files
                $sectionDocs[$key] = array();
                foreach($file as $content){
                    if($content != 'overview.md'){ //skip this one. The level above will link to it
                        $sectionDocs[$key][] = array(
                            'name' => basename($content, '.md'),
                            'url' => $router->generate('valhalla_core_utils_documentation',
                                    array(
                                        'section' => $key,
                                        'content' => basename($content, '.md'),
                                    )
                            ),
                        );
                    }
                }
            } 
        }
        
        return $sectionDocs;
    }
}