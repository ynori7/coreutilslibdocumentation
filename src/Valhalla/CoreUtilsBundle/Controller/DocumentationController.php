<?php

namespace Valhalla\CoreUtilsBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Valhalla\CoreUtilsBundle\Classes\Documentation;
use Valhalla\CoreUtilsBundle\Classes\Navigation;

class DocumentationController extends Controller
{
    public function initResponse($ttl = 3600){
        $response = new Response();
        $response->setPublic();
        $response->setMaxAge($ttl);
        $response->setSharedMaxAge($ttl);
        
        return $response;
    }

    
    public function pageAction($page) {
        $documentation = new Documentation($this->get('kernel')->getRootDir() . '/..');
        $markup = $documentation->getPage($page);
        
        $response = $this->initResponse();
        
        return $this->render('ValhallaCoreUtilsBundle:Pages:page.html.twig',
            array(
                'markup' => $markup,
                'currentPage' => $page,
            ),
            $response
        );
    }
    
    public function documentationAction($section, $content) {
        $documentation = new Documentation($this->get('kernel')->getRootDir() . '/..');
        $markup = $documentation->getSectionDocumentation($section, $content);
        
        $response = $this->initResponse();
        
        return $this->render('ValhallaCoreUtilsBundle:Pages:documentation.html.twig',
            array(
                'markup' => $markup,
                'currentPage' => 'docs',
                'currentSection' => $section,
                'currentContent' => $content,
            ),
            $response
        );
    }
    
    public function navigationAction($currentPage = 'overview', $currentSection = '', $currentContent = ''){
        $navigationClass = new Navigation($this->get('kernel')->getRootDir() . '/..');
        $navigation = $navigationClass->getNavigation($this->get('router'));
        
        $response = $this->initResponse(600);
        
        return $this->render('ValhallaCoreUtilsBundle:Includes:navigation.html.twig',
            array(
                'navigation' => $navigation,
                'currentPage' => $currentPage,
                'currentSection' => $currentSection,
                'currentContent' => $currentContent,
            ),
            $response
        );
    }
    
}
